jQuery(document).ready(function($){

  $('.contact-button').on('click', function(){
     $.fancybox.open({
        	src  : '#contact_form_pop',
        	type : 'inline',

      });
  });
  $( ".menu-item a:contains('Kontakt')" ).on('click', function(){
     $.fancybox.open({
        	src  : '#contact_form_pop',
        	type : 'inline',

      });
  });

  $('.slider').slick({
    dots: true,
    adaptiveHeight: true,
    arrows: false,
    cssEase: 'ease-in-out',
    speed: 750,
    // fade: true

  });


  count = 0;
  $('#color-move').on('click', function() {
     $('.slider').slick('slickNext');
      count += 1;
      if(count % 3 == 0){
        $.fancybox.open({
             src  : '#contact_form_pop',
             type : 'inline',

         });
      }
  });
  $('.slider').on('beforeChange', function(event, slick, currentSlide, nextSlide){

    var slideNum = nextSlide;
    // console.log(slideNum);
    if(slideNum == 1){
      $('#color-warp').attr('class', 'red');
    } else if (slideNum == 2){
      $('#color-warp').attr('class', 'pink');
    } else {
      $('#color-warp').attr('class', 'blue');
    }

    $(".slick-slide").removeClass('fadeOut fadeIn');
    $('.slick-current').addClass('fadeOut');

    // var NextSlideDom=$(slick.$slides.get(nextSlide));
    //
    // NextSlideDom.addClass('fadeIn')

  });


  $('.testislider').each(function() {
    var $this = $(this);
    if ($this.children().length > 1) {
        $this.slick({
            dots: true,
            adaptiveHeight: true,
            fade: true,
            prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left' aria-hidden='true'></i></button>",
            nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right' aria-hidden='true'></i></button>",
        });
    } else{
      $this.slick({
          dots: false,
          adaptiveHeight: true,
          fade: true,
          prevArrow:"<button type='button' class='slick-prev pull-left'><i class='fas fa-chevron-left' aria-hidden='true'></i></button>",
          nextArrow:"<button type='button' class='slick-next pull-right'><i class='fas fa-chevron-right' aria-hidden='true'></i></button>",
      });
    }
});

 $('#cancel').on('click', function(){
   $.fancybox.close();
 });

 $('#menu-button').on('click', function(){
   $('#header-menu').show('slide', {direction: 'left', distance: '1000'}, 500);
 })
 $('#close-menu').on('click', function(){
   $('#header-menu').hide('slide', {direction: 'left', distance: '1000'}, 500);
 });
 $(window).load(function() {
      $('.cookie-block').delay(500).show('slide', {direction: 'down', distance: '500'}, 500);
});

 $('#cookie_consent').on('click', function(){
    $('.cookie-block').hide('slide', {direction: 'down', distance: '500'}, 500);
 });

});

$(window).scroll(function() {
  var height = $(window).scrollTop();

  if((height  > 190)) {
       $("#primary-menu").addClass("trans");
  } else{
       $("#primary-menu").removeClass("trans");
  }
});
