<?php

 /* Template Name: Homepage */

/**
 * The template for displaying home page
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package patientuslanding
 */

get_header();

?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main ">
			<div class="topline">	</div>
			<div class="colored">

				<div class="firstblock">
					<div class="container slider-lead">
						<div class="leadblock">

							<?php the_field('lead');	?>
						</div>
						<?php if( have_rows('slider') ): ?>
						<!-- <div class="slide-container"> -->
						<div class="mainslider-container">
							<img id="ihr-logo-hier" src="<?php echo get_template_directory_uri() . '/img/ihr-logo-hier.svg'?>" />

							<div class="slider">
								<?php while( have_rows('slider') ): the_row(); ?>
									<?php
										$slider_item = get_sub_field('slider_item');
										if( $slider_item ):
										?>
												<div class="slide animated" data-color="<?php echo $slider_item['class'];?>">
													<div class="slide-cont">
														<div class="big-picture"> <img src="<?php echo $slider_item['big_picture']['url'];?>"></div>
														<div class="small-picture"><img src="<?php echo $slider_item['small_picture']['url'];?>"> </div>
													</div>
												</div>
										<?php endif; ?>
									<?php endwhile; ?>
								</div>
							<!-- </div> -->
							<?php endif; ?>
						</div>
					</div>
					<div class="container">
							<?php if( have_rows('main_buttons') ): ?>
								<div class="button-block">
									<div class="padding-container">
										<?php while( have_rows('main_buttons') ): the_row(); ?>
											<button class="green" id="color-move"> <?php the_sub_field('green_button') ;?></button>
											<button class="contact-button"> <?php the_sub_field('trans_button') ;?></button>
										<?php endwhile; ?>
									</div>
								</div>
							<?php endif; ?>
						</div>
					<div class="container">
							<?php if( have_rows('cookie') ): ?>
								<?php while( have_rows('cookie') ): the_row(); ?>
									<div class="cookie-block">
										<div class="padding-container">
											 <?php the_sub_field('cookie_text') ;?>
											 <span id="cookie_consent"><?php the_sub_field('cookie_button') ;?> <i class="fas fa-check"></i></span>
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
							<div id="contact_form_pop" class="lightbox">
									<?php echo do_shortcode('[contact-form-7 id="172" title="Main contact"]'); ?>
							</div>
						</div>
					<div class="container">
								<div class="video-block">
									<?php
									$video = get_field('video');

									if( $video ): ?>
									<div class="padding-container">
										<div class="video-instruction">
											<div class="text-instr">
												  <?php echo $video['video_block_title'];?>
											</div>
											<i id="arrow" class="fas fa-chevron-down"></i>
										</div>
									</div>
										<div class="videoscreen">
											<div class="video-container">
													<a href="<?php echo $video['videofile']['url'] ;?>" data-fancybox data-width="640" data-height="360"><img id="play-button" src="<?php echo get_template_directory_uri() . '/img/play.svg'?>" /></a>
												  <img class="screen" src="<?php echo $video['screen']['url'] ;?>" >
											</div>
											<span class="caption">
												<?php echo $video['caption'];?>
											</span>
										</div>
									<?php endif; ?>
									</div>
								</div>
							</div>
						</div>
				</div>
				<div class="white-block">
					<div class="whiteline"></div>
					<div class="container">

							<?php if( have_rows('online_block') ): ?>
								<?php while( have_rows('online_block') ): the_row(); ?>
									<div class="online-text-block">
										<div class="padding-container">
										<h3><?php echo get_sub_field('title');?></h></h3>
										<p><?php echo get_sub_field('text');?> </p>

										<?php if( have_rows('options') ): ?>
											<div class="options-block">
											<?php while( have_rows('options') ): the_row(); ?>
												<?php if( have_rows('option') ): ?>
													<div class="option">
														<?php while( have_rows('option') ): the_row(); ?>

															  <h4><?php the_sub_field('option_title'); ?></h4>
																	<?php if( have_rows('option_images') ): ?>
																		<?php while( have_rows('option_images') ): the_row(); ?>
																			<?php if( have_rows('option_img_group') ): ?>
																				<?php while( have_rows('option_img_group') ): the_row(); ?>
																					<?php	$image = get_sub_field('option_img'); ?>
																					<img class="<?php the_sub_field('option_img_color_id'); ?>" src="<?php echo $image['url'];?>" />
																				<?php endwhile; ?>
																			<?php endif; ?>
																		<?php endwhile; ?>
																	<?php endif; ?>
																<p><?php the_sub_field('option_caption'); ?> </p>
														<?php endwhile; ?>
													</div>
												<?php endif; ?>
											<?php endwhile; ?>
											</div>
										<?php endif; ?>
										</div>
									</div>
								<?php endwhile; ?>
							<?php endif; ?>
						</div>
					</div>
					</div>
					<div class="colored">
						  <div class="container">

								<?php if( have_rows('banners_block') ): ?>
									<div class="banners-block">
										<div class="padding-container">
										<?php while( have_rows('banners_block') ): the_row(); ?>

											<h3><?php the_sub_field('banners_block_title'); ?> </h3>
											<?php the_sub_field('banners'); ?>

										<?php endwhile; ?>
										</div>
								</div>
								<?php endif; ?>
								<?php if( have_rows('features-block') ): ?>
									<div class="features-block">
										<div class="padding-container">
										<ul >
										<?php while( have_rows('features-block') ): the_row(); ?>

												<?php if( have_rows('feature') ): ?>
													<?php while( have_rows('feature') ): the_row(); ?>
													  <li class="feature-item ">
															<div class="container10">
															  <h3><h3><?php the_sub_field('feature_title'); ?> </h3></h3>

																<?php if( have_rows('feature_imgs_groups') ): ?>
																	<?php while( have_rows('feature_imgs_groups') ): the_row(); ?>
																		<?php if( have_rows('feature_img_set') ): ?>
																			<?php while( have_rows('feature_img_set') ): the_row(); ?>
																				<?php	$fimage = get_sub_field('feature_img'); ?>
																				<div class="fe-img-container">
																					<img class="<?php the_sub_field('feature_img_class'); ?>" src="<?php echo $fimage['url'];?>" />
																				</div>

																			<?php endwhile; ?>
																		<?php endif; ?>
																	<?php endwhile; ?>
																<?php endif; ?>


																<?php the_sub_field('feature_caption'); ?>
															</div>
														</li>
													<?php endwhile; ?>
												<?php endif; ?>

										<?php endwhile; ?>
										</ul>
									</div>
									</div>
								<?php endif; ?>
								<?php if( have_rows('testimonies_slider') ): ?>
										<div class="testimonies-block">
												<div class="testimonies-container">
														<div class="testislider">
															<?php while( have_rows('testimonies_slider') ): the_row(); ?>
															<div class="testimonieslide">
																	<?php
																	$testimonieslide = get_sub_field('testimonie_slide');
																	if( $testimonieslide ): ?>
																		<div class="slider-top">
																			<img class="face" src="<?php echo $testimonieslide['photo']['url']; ?>" />
																			<p class="name"><?php echo $testimonieslide['name']; ?></p>
																			<p class="spec"><?php echo $testimonieslide['category']; ?></p>
																		</div>
																		<?php echo $testimonieslide['review']; ?>
																	<?php endif; ?>
															</div>
															<?php endwhile; ?>
													</div>
												</div>

										</div>
									<?php endif; ?>
								</div>
							</div>
							<div class="contactblock-wraper">
								<div class="grayline"></div>
								<div class="container">
									<?php if( have_rows('contact_block') ): ?>
										<div class="contact">
											<div class="padding-container">
											<?php while( have_rows('contact_block') ): the_row(); ?>

											  <h3><?php the_sub_field('contact_title'); ?></h3>
												<?php $photo = get_sub_field('contact_photo'); ?>
												<img class="photo" 	src="<?php echo $photo['url']; ?> " />
												<p class="name"><b><?php the_sub_field('contact_name'); ?></b></p>
												<p class="spec"><?php the_sub_field('contact_position'); ?></p>

												<button class="green contact-button"><?php the_sub_field('contact_button'); ?></button>

												<?php the_sub_field('contact_contact'); ?>
											<?php endwhile; ?>
										</div>
										</div>
									<?php endif; ?>
							</div>
					</div>
				</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_sidebar();
get_footer();
