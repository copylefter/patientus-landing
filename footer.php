<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package patientuslanding
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer gray">


		<div class="container">

			<div class="site-info">
				<div class="custom">
					<div class="padding-container">
					<?php
					if ( function_exists('dynamic_sidebar') )
						dynamic_sidebar('footer');
					?>

				</div>
				</div>
				

			</div><!-- .site-info -->


		</div>
	</footer><!-- #colophon -->

</div><!-- #page -->

<?php wp_footer(); ?>
</div> <!-- colorwarp -->
</body>
</html>
