<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package patientuslanding
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">

	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="color-warp" class="blue">
<div id="page" class="site">

		<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'patientuslanding' ); ?></a>
	<div class="container header-container">

		<header id="masthead" class="site-header header  color main-container">

			<nav id="site-navigation" class="main-navigation ">

				<img id="menu-button" src="<?php echo get_template_directory_uri() . '/img/menu-icon.svg'?>" />

				<!-- <button class="menu-toggle" aria-controls="header-menu" aria-expanded="false"><?php esc_html_e( 'Primary Menu', 'patientuslanding' ); ?></button> -->
				<div id="header-menu">

					<i id="close-menu" class="fas fa-times"></i>
						<div class="contact-info">
							<p><b>KONTAKT:</b></p>
							<p>Senior Key Account Manager</p>
							<p><b><a href="#"> 030-2201 390 78</a></b></p>
							<p><a href="#">christoph.walz@patientus.de</a></p>
						</div>

				<?php
				wp_nav_menu( array(
					'theme_location' => 'menu-1',
					'menu_id'        => 'primary-menu',
				) );
				?>
				</div>
			</nav><!-- #site-navigation -->
		</header><!-- #masthead -->
		<div class="site-branding">
			<img id="logo" src="<?php echo get_template_directory_uri() . '/img/logo.svg'?>">
		</div><!-- .site-branding -->
</div>
		<div id="content" class="site-content">
